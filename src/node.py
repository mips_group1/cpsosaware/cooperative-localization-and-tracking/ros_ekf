#!/usr/bin/env python3

import rospy
from rospy.numpy_msg import numpy_msg

from derived_object_msgs.msg import ObjectArray
from geometry_msgs.msg import PoseStamped,TransformStamped
from nav_msgs.msg import Path
import message_filters
from extended_kalman.msg import NeighborList, NeighborInfo, StampedFLoatArray
from std_msgs.msg import Int16, Float64, Header
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

import math
import random
from numpy.linalg import norm, inv
from scipy.sparse.linalg import lsmr
import tf2_ros

from scipy.spatial.transform import Rotation as R


###### Mean and variance of white Gaussian noise ######
    

class Estimator:
    def __init__(self): 
        
        
         self.br_input = tf2_ros.TransformBroadcaster()
         self.t_input=TransformStamped()
        
         self.br = tf2_ros.TransformBroadcaster()
         self.t=TransformStamped()         
         
               
         self.dso_mode=False
         #threshold in meters for detecting neighbors
         self.neighbors_threshold=70
         
         #list containing the vehicles in the scene
         self.vehicles=[]
         
         self.initial_callback=True
         self.to_define_ego_id=True
         
         
         self.neighbors_mat=[0,0]
         
         self.mhi_alone_KF=[0,0]
         self.mhi_local_KF=[0,0]
         self.Sigma_local_KF=[0,0]
         self.Sigma_alone_KF=[0,0]
         self.Deg=[0,0]
         
         #Error lists
         self.ekf_alone_error=[]
         self.individual_dll_error=[]
         self.individual_dcekf_error=[]
         
         #parameters
         self.sigma_x=3.5
         self.sigma_y =3.5
         self.sigma_gyro = 0.2
         self.sigma_d =0
         self.sigma_a = 0
         
         #callback counter
         self.counter=0
         self.real_frame_count=0
         
         
         # r1/fps
         self.prev_time=0
         self.Dt=0.033
#         
#         
         #id of the ego_vehicle
         self.ego_id=0
         
         
         #init dso pose
         self.map_to_dso=None
         
         #neigbors id publisher
#         self.neigbors_list_publisher=rospy.Publisher('ekf/neighbors',IntListStamped, queue_size=1)
         self.neighbors=[]
         
         self.neigbors_list_publisher = rospy.Publisher('ekf/neighbors', NeighborList,queue_size=10)
         
         
         #Pose publisher
         self.pose_pub = rospy.Publisher('ekf/pose', PoseStamped, queue_size=1)
         
         
         self.path_pub=rospy.Publisher('ekf/path',Path, queue_size=1)
         
         self.path=  Path()
         self.path.header.frame_id = "/map"
         self.path.header.stamp=rospy.Time.now()
         
         
         #Lists for files
         self.input_list=[]
         self.output_list=[]
         
         
         self.tBuffer=tf2_ros.Buffer();
         self.listener=tf2_ros.TransformListener(self.tBuffer)
        
         
         #subscribe to this topic only to get the of ego 
         rospy.Subscriber("carla/ego_vehicle/objects", ObjectArray, self.ego_callback)

         
         #subscribes to the topic that publishes the information about the vehicles
         rospy.Subscriber("/carla/objects", ObjectArray, self.callback)    
         
         
         self.number_of_neighbors_pub=rospy.Publisher("/ekf/number_of_neighbors", Int16, queue_size=10)
         
         self.individual_gps_error_pub = rospy.Publisher("/GPS", Float64, queue_size = 10)

         self.individual_dll_error_pub = rospy.Publisher("/DLL-SA", Float64, queue_size = 10)

         self.individual_lkf_error_pub = rospy.Publisher("/LKF-SA", Float64, queue_size = 10)

         self.individual_gps_error_array_pub = rospy.Publisher("/ekf/gps_error/array", StampedFLoatArray, queue_size = 10)

         self.individual_dll_error_array_pub = rospy.Publisher("/ekf/dll_error/array", StampedFLoatArray, queue_size = 10)

         self.individual_lkf_error_array_pub = rospy.Publisher("/ekf/lkf_error/array", StampedFLoatArray, queue_size = 10)
        
         self.gps_ca_error_pub = rospy.Publisher("/ekf/gps_ca_error/array", StampedFLoatArray, queue_size = 10)

         self.lkf_ca_error_pub = rospy.Publisher("/ekf/lkf_ca_error/array", StampedFLoatArray, queue_size = 10)

         self.first_pass = True
         

         self.ax=[]  
         self.ay=[]
         self.bx=[]   
         self.by=[]

         self.x_GPS_CDF = []
         self.y_GPS_CDF = []

         self.gps_error = []

         self.lkf_ca_error = []

         self.gps_ca_error = []

    
         
         
         
    def ego_callback(self, data):

        if self.to_define_ego_id:
            #The  first with 6 is always the ego
            self.ego_id=[x.id for x in data.objects if x.classification==6][0]

            self.to_define_ego_id=False
            print("The id of the ego vehicle is {}".format(self.ego_id))
    


    def callback(self, data):
        
        



        #If we don't know the id of the ego, return
        if self.ego_id==0:
            print("No ego_id")
            return
        
        self.Dt=data.header.stamp.to_sec()-self.prev_time
#        print(data.header.stamp.to_sec()-self.prev_time)
        self.prev_time=data.header.stamp.to_sec()
       
        if(self.dso_mode==True):
            try:
                dso_pose=self.tBuffer.lookup_transform("map","dso/cam",data.header.stamp,rospy.Duration(1.0)) 
            except(tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros. ExtrapolationException):
                print("exception")
                return
        
#        self.map_to_dso=map_to_dso
        
        
        #Initialize the pose
        p=PoseStamped()
        
        p.header.stamp=data.header.stamp
        p.header.frame_id='map'
#        p.pose.orientation.x = self.map_to_dso.transform.rotation.x #self.dso_pose.orientation.x
#        p.pose.orientation.y = self.map_to_dso.transform.rotation.y
#        p.pose.orientation.z = self.map_to_dso.transform.rotation.z
#        p.pose.orientation.w = self.map_to_dso.transform.rotation.w
#        p.pose.position.z = self.map_to_dso.transform.translation.z
#        

        
        # All the vehicles are classified with the id=6 (vehcles), 7(trucks), 8(motorcycles)
        #Select all the vehicles from the object in the scene
        self.vehicles=[x for x in data.objects if (x.classification==6 or x.classification==7 or x.classification==8)]
        if self.initial_callback!=0:

            #number of vehicles x 8 array
            #id--x--y--noisy_x--noisy_y--noisy_yaw--noisy_velocity_xy--noisy_angular
            
            self.values=np.zeros((len(self.vehicles),8 ),np.object)
            self.initial_callback=False
            
            self.individual_gps_error=np.zeros((len(self.vehicles),8),np.object)
            # self.Dt=0.033
           
    

        # enumerate vehicles and fill the self.values array
        for i,v in enumerate(self.vehicles):
            #carla id of the vehicle
            self.values[i,0]=v.id
            
            # true position
            self.values[i,1]=v.pose.position.x
            self.values[i,2]=v.pose.position.y
           
            #noisy x,y
            self.values[i,3:5]=self.add_gps_error(v.pose.position.x,v.pose.position.y)
            
           
           #get euler from quaternion
            r = R.from_quat([v.pose.orientation.x,v.pose.orientation.y,v.pose.orientation.z,v.pose.orientation.w ])
           #get yaw
            self. values[i,5]=r.as_euler('zyx', degrees=True)[0]
            
            #get velocity+noise
            self.values[i,6]=self.add_noise_to_velocity(v.twist.linear.x, v.twist.linear.y)
            
            #get angular_z+noise
            self.values[i,7]=self.add_noise_to_angular_z(v.twist.angular.z)
            
            
            #chache gps error for every vehicle
            self.individual_gps_error[i] = norm(np.array([self.values[i,1]-self.values[i,3] ,self.values[i,2]-self.values[i,4]]), 2)
            if(v.id==self.ego_id):            
                p.pose.orientation.x = v.pose.orientation.x#self.dso_pose.orientation.x
                p.pose.orientation.y = v.pose.orientation.y
                p.pose.orientation.z = v.pose.orientation.z
                p.pose.orientation.w = v.pose.orientation.w
                p.pose.position.z = v.pose.position.z
              
        
        #Create neighbor message
        neighbor_msg=NeighborList()  
        
        
        ### Find the index of ego
        self.ego_index = np.where(self.values[:,0]==self.ego_id)[0][0]
        
        if(self.dso_mode==False):
        ## Noisy ego pose
            noisy_ego_x=self.values[self.ego_index,1]+np.random.normal(0, self.sigma_x)
            noisy_ego_y=self.values[self.ego_index,2]+np.random.normal(0, self.sigma_y)

            #noisy_ego_x=self.values[self.ego_index,1]+ self.n
            #noisy_ego_y=self.values[self.ego_index,2]+ self.n

            # self.n += 0.01*random.uniform(-1.01, 1.05)
        else:
            
        #using dso
            noisy_ego_x=dso_pose.transform.translation.x
            noisy_ego_y=dso_pose.transform.translation.y
            
        temp_noisy_ego_x = []
        temp_noisy_ego_y = []

        temp_noisy_ego_x.append(noisy_ego_x)

        temp_noisy_ego_y.append(noisy_ego_y)
        

        self.gps_error.append( norm(np.array([ self.values[self.ego_index,1] - noisy_ego_x , self.values[self.ego_index,2] - noisy_ego_y ])) )
        ### Retrive locations, velocities etc. for all vehicle of the current time instant ###    
            
        ### Find the neighbors of individual vehicle at current time instant ###
            
        self.Deg[0]=self.Deg[-1]
        self.A, self.Deg[-1], self.neighbors, neighbor_idxs = self.Create_Adjacency_and_Degree(np.array(self.values[:,0:3],dtype=np.float64), self.ego_index)
        
        
#        int_n=self.neighbors.astype(int)
#        self.neigbors_msg.data=numpy_msg(self.neighbors)
#        self.neigbors_msg.size=len(self.neighbors)
#        self.neigbors_msg.label="ego neighbors"
#                   
        
        ### Find the neighbors of individual vehicle at current time instant ###
    
        ### Initialize Kalman for the individual vehicle at current time instant ###
        self.neighbors_mat[0]=self.neighbors_mat[-1]
        
        self.mhi_alone_KF[0]=self.mhi_alone_KF[1]
        self.mhi_local_KF[0]=self.mhi_local_KF[1]
        self.Sigma_alone_KF[0]=self.Sigma_alone_KF[1]
        self.Sigma_local_KF[0]=self.Sigma_local_KF[1]
        self.mhi_local_KF[-1], self.Sigma_local_KF[-1],self.mhi_alone_KF[-1],self.Sigma_alone_KF[-1],self.neighbors_mat[-1] = self.Initialize_Kalman(self.Deg, self.A,  self.ego_index)
#        print("Aftetr init  mhi_local_KF is {}".format(self.mhi_local_KF))

        
        ## Initialize Kalman for the individual vehicle at current time instant ###
        if (self.Deg[-1] == 0.0): ### if vehicle has no neighbors, then perform single EKF  


            ekf_alone_error_last, mhi_alone_KF_last, Sigma_alone_KF_last, est_x,est_y = self.EKF_with_GPS(self.mhi_alone_KF[0], 
                           self.Sigma_alone_KF[0], 
                           self.Deg[0],
                           self.values[self.ego_index,1], #  true_x 
                           self.values[self.ego_index,2], # true_y
                           self.values[self.ego_index,5], # noisy_heading
                           self.values[self.ego_index,6], # noisy_velocity
                           self.values[self.ego_index,7], # noisy_angular_z
                           self.sigma_x, 
                           self.sigma_y,
                           self.counter,
                           noisy_ego_x,
                           noisy_ego_y)
          
            #Add to lists
            self.mhi_alone_KF[1]=mhi_alone_KF_last
                
            self.Sigma_alone_KF[1]=Sigma_alone_KF_last
            self.ekf_alone_error.append(ekf_alone_error_last)

                
#            self.individual_dll_error[self.counter] = self.individual_gps_error[self.ego_index]## DLL can't be performed, i.e. its error is equal to that of GPS
                    
#            self.individual_dcekf_error[s to float for intermediate computations then cast back to uint8 for output/display purposes. As poisson noise is all >=0, you will need to decide how you want to handle overflow of your arrays as you cast back to uint8. You could scale or truncate depending on whaelf.counter] = self.ekf_alone_error[self.counter]
            number_of_neighbors=0
            p.pose.position.x=est_x
            p.pose.position.y=est_y
            
            self.individual_dll_error.append(self.gps_error[-1])
            self.individual_dcekf_error.append(ekf_alone_error_last)
#            print("If it has no neighbors mhi_local_KF is {}".format(self.mhi_local_KF))
        else:
          
            delta_X,delta_Y,test_Points,test_noise_Points,delta_X_from_neighbors,delta_Y_from_neighbors,neighborhood_heading, neighborhood_velocity,neighborhood_angular, neighbors_gps_error  = self.Initialize_Parameter_for_Location_Estimation(
                                                                                 self.neighbors_mat[-1], 
                                                                                 self.ego_index,
                                                                                 self.values[:,1], #true_x
                                                                                 self.values[:,2], #true_y
                                                                                 self.sigma_d, 
                                                                                 self.sigma_a,
                                                                                 self.sigma_x, 
                                                                                 self.sigma_y, 
                                                                                 self.Deg[-1], 
                                                                                 self.values[:,5], #heading
                                                                                 self.values[:,6], #velocity
                                                                                 self.values[:,7],#angular
                                                                                 noisy_ego_x,
                                                                                 noisy_ego_y
                                                                                 )        
            neighbors_mat_current = self.neighbors_mat[1] 

                    
            neighbors_mat_previous = self.neighbors_mat[0]
            individual_dll_error_last,individual_dcekf_error_last,mhi_local_KF_last, Sigma_local_KF_last, est_x, est_y = self.Distributed_Lapl (
                    neighbors_mat_current, 
                          neighbors_mat_previous, 
                          self.mhi_local_KF[0], 
                          self.Sigma_local_KF[0], 
                          self.counter, 
                          self.Deg[1], 
                          delta_X, 
                          delta_Y, 
                          test_noise_Points[:,0], 
                          test_noise_Points[:,1], 
                          test_Points[0], 
                          test_Points[1], 
                          delta_X_from_neighbors, 
                          delta_Y_from_neighbors, 
                          neighborhood_heading,
                          neighborhood_velocity, 
                          neighborhood_angular)
            
            #Add to lists

            self.mhi_local_KF[1]=mhi_local_KF_last
            self.individual_dll_error.append(individual_dll_error_last)
            self.individual_dcekf_error.append(individual_dcekf_error_last)
            self.Sigma_local_KF[1]=Sigma_local_KF_last
#            print("If it has neighbors  mhi_local_KF is {}".format(self.mhi_local_KF))
            
#            print("the number of neighbors is {}".format(number_of_neighbors))
#            print(self.mhi_local_KF[0].shape[0])
#            print(self.mhi_local_KF)
#            p1=np.array([self.mhi_local_KF[0][0], self.mhi_local_KF[0][number_of_neighbors]])
#            p2=np.array([self.values[self.ego_index,1],self.values[self.ego_index,2]])
#            err=norm(p1-p2)
#            print(err)            
            
       
            p.pose.position.x = est_x
            p.pose.position.y = est_y
            
        number_of_neighbors=int((self.mhi_local_KF[-1].shape[0]/3)-1)
        
        if(number_of_neighbors>0):
            vector=self.mhi_local_KF[-1]

            neighbor_xs= vector[1:number_of_neighbors+1]
            neihgbor_ys= vector[(number_of_neighbors+2):(2*number_of_neighbors+2)]
            
            
#            print(number_of_neighbors)
#            print(vector)
#            print(neighbor_xs)
#            print(neihgbor_ys)
#            print(self.mhi_local_KF)
#            print(self.neighbors)
            ee=0
            for n, x, y,e  in zip(self.neighbors, neighbor_xs,neihgbor_ys, neighbors_gps_error):
                k=NeighborInfo()
                k.x=x
                k.y=y


#                print("Estimated x: {0} , true x: {1}, estimated y: {2}, true y: {3}".format(x,y,n[1],n[2]))
                
                k.ate_error=round(np.linalg.norm(n[1:3]-np.array([x,y])),2)
#                print(k.ate_error)
                ee=ee+k.ate_error
                k.id=int(n[0])
                k.gps_error=e
                neighbor_msg.neighbors.append(k)
                    #publish neighbors list

            neighbors_gps_error.append(self.gps_error[-1])      
            gps_mean=np.mean(neighbors_gps_error)
            ca_mean=float((ee + individual_dcekf_error_last)/(number_of_neighbors+1))
            self.lkf_ca_error.append(ca_mean)
            self.gps_ca_error.append(gps_mean)
            print("The gps mean is {0} and the ca mean is {1}".format(round(gps_mean,2), round(ca_mean,2)))
        else:

            self.lkf_ca_error.append(self.individual_dcekf_error[-1])
            self.gps_ca_error.append(self.gps_error[-1])

#        # Make sure the quaternion is valid and normalized
##        print(self.mhi_local_KF[-1][0])
##        print(self.mhi_local_KF[-1].shape)
##        print(self.neighbors)
#        vector=self.mhi_local_KF[-1]
##        print(vector)
##        print(type(vector))
##        print(vector[0])
##        print(type(vector[0]))
##       
#        if(self.neighbors.shape[0]>0):
#            neighbors_size=self.neighbors[0].shape[0]
#            print(neighbors_size)
#            
#            neighbor_xs= vector[1:neighbors_size+1]
#            neihgbor_ys= vector[(neighbors_size+2):(2*neighbors_size+2)]
#    
#            neighbor_msg=NeighborList()
#            
#            for n, x, y  in zip(self.neighbors, neighbor_xs,neihgbor_ys):
#                k=NeighborInfo()
#                k.x=x
#                k.y=y
#                print(n)
#                print(neighbor_xs)
#                print(neihgbor_ys)
#                print("Estimated x: {0} , true x: {1}, estimated y: {2}, true y: {3}".format(x,y,n[1],n[2]))
#               
#                k.ate_error=round(np.linalg.norm(n[1:3]-np.array(x,y)),2)
#                print(k.ate_error)
#    
#                k.id=n[0]
#                neighbor_msg.neighbors.append(k)
#                    #publish neighbors list
#            self.neigbors_list_publisher.publish(neighbor_msg)
       
        self.t.header.stamp = data.header.stamp
        self.t.header.frame_id = "map"
        self.t.child_frame_id = "ekf/pose"
        self.t.transform.translation.x = p.pose.position.x
        self.t.transform.translation.y = p.pose.position.y
        self.t.transform.translation.z = p.pose.position.z
        self.t.transform.rotation.x =  p.pose.orientation.x
        self.t.transform.rotation.y =  p.pose.orientation.y
        self.t.transform.rotation.z = p.pose.orientation.z
        self.t.transform.rotation.w =  p.pose.orientation.w
        #send transform
        self.br.sendTransform(self.t)
        
        
        #input
        self.t_input=self.t
        self.t_input.child_frame_id = "ekf/input"
        self.t_input.transform.translation.x =noisy_ego_x
        self.t_input.transform.translation.y = noisy_ego_y
        
        self.br.sendTransform(self.t_input)
        
        
        
        self.path.poses.append(p)
        self.path_pub.publish(self.path)

        self.pose_pub.publish(p)
        
        self.neigbors_list_publisher.publish(neighbor_msg)

        self.number_of_neighbors_pub.publish(number_of_neighbors)
        
        self.individual_gps_error_pub.publish(self.gps_error[-1])
        
        self.individual_dll_error_pub.publish(self.individual_dll_error[-1])

        self.individual_lkf_error_pub.publish(self.individual_dcekf_error[-1])

    
        # self.gps_ca_error_pub.publish(self.gps_ca_error[-1])

        # self.lkf_ca_error_pub.publish(self.lkf_ca_error[-1])

        h=Header()
        h.frame_id="map"
        h.stamp=rospy.Time.now()
        h.seq=self.counter



        if(len(self.gps_error)>200):
            msg1=StampedFLoatArray()
            msg1.header=h
            msg1.data=self.individual_dll_error[-200:]
            msg2=StampedFLoatArray(header=h,data=self.gps_error[-200:])
            msg3=StampedFLoatArray(header=h,data=self.individual_dcekf_error[-200:])
            msg4=StampedFLoatArray(header=h,data=self.gps_ca_error[-200:])
            msg5=StampedFLoatArray(header=h,data=self.lkf_ca_error[-200:])
        else:
            msg1=StampedFLoatArray()
            msg1.header=h
            msg1.data=self.individual_dll_error
            msg2=StampedFLoatArray(header=h,data=self.gps_error)
            msg3=StampedFLoatArray(header=h,data=self.individual_dcekf_error)
            msg4=StampedFLoatArray(header=h,data=self.gps_ca_error[-200:])
            msg5=StampedFLoatArray(header=h,data=self.lkf_ca_error[-200:])

        #print(msg1)

        self.individual_dll_error_array_pub.publish(msg1)
        self.individual_gps_error_array_pub.publish(msg2)
        self.individual_lkf_error_array_pub.publish(msg3)
        self.gps_ca_error_pub.publish(msg4)
        self.lkf_ca_error_pub.publish(msg5)


        self.counter+=1   

        return


        #self.x_GPS_CDF=[]
        #self.y_GPS_CDF=[]
        self.x_GPS_CDF = np.sort(np.asarray(self.gps_error))
        self.y_GPS_CDF = np.arange(len(self.gps_error))/(len(self.gps_error))

        if (self.first_pass == True):

            plt.ion()

            fig = plt.figure()

            # plt.axis([ 0, 1000, 0, 1 ])
            plt.plot(np.sort(self.individual_dcekf_error), np.arange(len(self.individual_dcekf_error))/len(self.individual_dcekf_error), 'm-', label = 'LKF-SA')

            plt.plot(np.sort(self.individual_dll_error), np.arange(len(self.individual_dll_error))/len(self.individual_dll_error), 'c-', label = 'DLL-SA')

            plt.plot(np.sort(self.gps_error), np.arange(len(self.gps_error))/len(self.gps_error), 'r-', label = 'GPS')

            plt.xlabel('Ego localization error [m]')

            plt.ylabel('CDF')

            plt.grid(b = True)

            plt.legend(facecolor = 'white')

            plt.show()

            plt.pause(0.0001)

            self.first_pass = False

        else:
            plt.clf()
            #print(self.x_GPS_CDF)
            #print(self.y_GPS_CDF)
            plt.plot(np.sort(self.individual_dcekf_error), np.arange(len(self.individual_dcekf_error))/len(self.individual_dcekf_error), 'm-', LABEL = 'LKF-SA')

            plt.plot(np.sort(self.individual_dll_error), np.arange(len(self.individual_dll_error))/len(self.individual_dll_error), 'c-', label = 'DLL-SA')

            plt.plot(np.sort(self.gps_error), np.arange(len(self.gps_error))/len(self.gps_error), 'r-', label = 'GPS')

            plt.xlabel('Ego localization error [m]')

            plt.ylabel('CDF')

            plt.grid(b = True)

            plt.legend(facecolor = 'white')

            plt.show()

            plt.pause(0.0001)


        # self.update_line(self.plt, [2,3,4], [3,2,2])
        
        #plt.clf()    

                  
        
        # num=0  
        # if (self.first_pass == True):

        #     plt.ion()    
        #     # plt.rcParams['savefig.dpi'] = 200 
        #     # plt.rcParams['figure.dpi'] = 200 
        #     plt.rcParams['figure.figsize'] = (10, 10)        
        #     plt.rcParams['font.sans-serif']=['SimHei']   
        #     plt.rcParams['axes.unicode_minus'] = False
        #     plt.rcParams['lines.linewidth'] = 0.5   
        #     plt.suptitle("TITLE",fontsize=30)


        #     self.first_pass=False
            
        #     while num<1:
        #         plt.clf()    
                            
        #         g1=np.random.random()  
                
        #         self.ax.append(num)      
        #         self.ay.append(g1)       
        #         agraphic=plt.subplot(2,1,1)
        #         #agraphic.set_title('TABLE1')      
        #         agraphic.set_xlabel('Location error',fontsize=20)   
        #         agraphic.set_ylabel('CDF', fontsize=20)
        #         plt.plot(self.ax,self.ay,'g-')                
        #         #table2
        #         # bx.append(num)
        #         # by.append(g1)
        #         # bgraghic=plt.subplot(2, 1, 2)
        #         # #bgraghic.set_title('TABLE2')
        #         # bgraghic.plot(bx,by,'r^')

        #         # plt.pause(0.4)     
            
        #         num=num+1
                

        #     plt.ioff()       
        #     plt.show()  

        # else:
        #     while num<1:
                
        #         plt.clf()    
                            
        #         g1=np.random.random()  
        #         print("1")
        #         self.ax.append(np.random.random() )      
        #         self.ay.append(g1)       
        #         agraphic=plt.subplot(2,1,1)
        #         #agraphic.set_title('TABLE1')      
        #         agraphic.set_xlabel('Location error',fontsize=20)   
        #         agraphic.set_ylabel('CDF', fontsize=20)
        #         plt.plot(self.ax,self.ay,'g-')                
        #         # #table2
        #         # bx.append(num)
        #         # by.append(g1)
        #         # bgraghic=plt.subplot(2, 1, 2)
        #         # #bgraghic.set_title('TABLE2')
        #         # bgraghic.plot(bx,by,'r^')

        #         plt.pause(0.4)     
            
        #         num=num+1

        #     plt.ioff()       
        #     plt.show()       


           
        
        
    def add_gps_error(self,true_x,true_y): 
        noisy_x=true_x + np.random.normal(0,self.sigma_x)
        noisy_y= true_y+ np.random.normal(0,self.sigma_y)
        return np.array([noisy_x,noisy_y])
      
        

    #Adds noise to angular velocity on the z(vertical) axis
    def add_noise_to_angular_z(self, ang_z, dev=0.2):
        #ang_z=0.0174532925*ang_z
        return ang_z + np.random.normal(0, math.radians(dev))
    
    #Adds noise to xy linear velocity
    def add_noise_to_velocity(self, vel_x, vel_y, factor=0.1):
        n_vel_x = np.random.normal(0, factor*abs(vel_x))
        
        n_vel_y = np.random.normal(0, factor*abs(vel_y))
        
        vel = math.sqrt( (vel_x + n_vel_x)**2 + (vel_y + n_vel_y)**2 )
        return vel
    
    def update_line(self, hl,data_x, data_y):
        hl.set_xdata(np.asarray(data_x))
        hl.set_ydata(np.asarray(data_y))

        plt.draw()
        plt.show()
    
    
            
    def CalculateAzimuthAngle (self, x_observer, x_target, y_observer, y_target):
      
        if (x_observer == x_target):
            
            if (y_observer < y_target):
                
                return math.radians(0)
            
            else:
                
                return math.radians(180)
            
        if (y_observer == y_target):
            
            if (x_observer < x_target):
                
                return math.radians(90)
            
            else:
                
                return math.radians(270)
            
        if (x_target > x_observer and y_target > y_observer):
            
            a = (math.atan((x_target-x_observer)/(y_target-y_observer)))
            
        elif (x_target > x_observer and y_target < y_observer):
            
            a = math.radians(90) + (math.atan((y_observer-y_target)/(x_target-x_observer)))
            
            
        elif (x_target < x_observer and y_target < y_observer):
            
            a = math.radians(180) + (math.atan((x_observer-x_target)/(y_observer-y_target)))
            
            
        elif (x_target < x_observer and y_target > y_observer):
            
            a = math.radians(270) + (math.atan((y_target-y_observer)/(x_observer-x_target)))
            
        
        return a
    
    
    def EKF_with_GPS(self, temp_mhi_alone_KF, temp_Sigma_alone_KF, ind_Deg, true_X, true_Y, heading, velocity, angular,  sigma_x, sigma_y,counter, noisy_x, noisy_y):
       
        ############ Input ############
        
        ############ temp_mhi_alone_KF: 2D location vector estimated the previous time instant ############
        ############ temp_Sigma_alone_KF: 2x2 covarinace matrix estimated the previous time instant ############
        ############ ind_Deg: number of neighbors the previous time instant (scalar) ############
        ############ true_X: true x-location of vehicle the current time instant (scalar) ############
        ############ true_Y: true y-location of vehicle the current time instant (scalar) ############
        ############ heading: yaw measurement from IMU (scalar) ############
        ############ velocity: velocity measurement from IMU (scalar) ############
        ############ angular: angualr measurement from IMU (scalar) ############
        ############ idx_time: time instant (scalar) ############
        ############ sigma_x: std of GPS noise (scalar) ############
        ############ sigma_y: std of GPS noise (scalar) ############
        
        ############ Output ############
        
        ############ ekf_error: single Kalman-based error of location estimation ############
        ############ mhi_alone_KF: 3D estimated location by signle Kalman ############
        ############ Sigma_alone_KF: 3x3 estimated covariance matrix by signle Kalman ############
        
        ### Initialize with zeros ###
        mhi_alone_KF = np.zeros(3)
        
        Sigma_alone_KF = np.zeros((3,3))
        
        mhi_alone_hat_KF = np.zeros(3)
        
        Sigma_alone_hat_KF = np.zeros((3,3))
        ### Initialize with zeros ###
        
        
        if (counter ==0): ### if the simulation has just started, then set to GPS
            mhi_alone_KF[0] =noisy_x ## x-measurement of GPS
            mhi_alone_KF[1] =noisy_y ## y-measurement of GPS
            mhi_alone_KF[2] = heading ## yaw measurement from IMU
            
            Sigma_alone_KF = np.eye(3)
                                
            temp_X = mhi_alone_KF[0]
            temp_Y = mhi_alone_KF[1]
                                    
                    
            ekf_error = norm(np.array([temp_X - true_X, temp_Y - true_Y])) ## location estimation error
                    
        else:
            
            
            state = 3
                    
            if (ind_Deg == 0): ## if vehicle had no neighbors the previous time instant, then continue executing the same single Kalman
                        
                ## Initialize the corresponding matrices of Kalman 
                G_alone = np.eye(state)
                
                if (angular != 0 ):
                      
                    G_alone[0,2] = ((velocity)/(angular))*np.cos(temp_mhi_alone_KF.T[2]) - ((velocity )/(angular))*np.cos(temp_mhi_alone_KF.T[2] + angular*self.Dt) 
                    G_alone[1,2] = ((velocity)/(angular))*np.sin(temp_mhi_alone_KF.T[2]) - ((velocity )/(angular))*np.sin(temp_mhi_alone_KF.T[2] + angular*self.Dt) 
                    
                else:
                        
                    G_alone[0,2] = (-velocity)*np.sin(temp_mhi_alone_KF.T[2])*self.Dt
                    G_alone[1,2] = (velocity)*np.cos(temp_mhi_alone_KF.T[2])*self.Dt
                        
                    
                H_alone = np.eye(state)
                         
                Q_alone = np.eye(state)
                
                R_alone = np.eye(state)
                ## Initialize the corresponding matrices of Kalman
                
                if (angular != 0):
                    
                    mhi_alone_hat_KF[0] = temp_mhi_alone_KF.T[0] + (-(velocity)/(angular))*np.sin(temp_mhi_alone_KF.T[2]) + ((velocity )/(angular))*np.sin(temp_mhi_alone_KF.T[2] + angular*self.Dt) ## update step with previously estimated x-location and x-part of kinematic model
                                            
                    mhi_alone_hat_KF[1] = temp_mhi_alone_KF.T[1] + ((velocity)/(angular))*np.cos(temp_mhi_alone_KF.T[2]) - ((velocity )/(angular))*np.cos(temp_mhi_alone_KF.T[2] + angular*self.Dt) ## update step with previously estimated y-location and y-part of kinematic model
                    
                    mhi_alone_hat_KF[2] = temp_mhi_alone_KF.T[2] + angular*self.Dt ## update step with previously estimated yaw and yaw-part of kinematic model
                
                else:
                    
                    mhi_alone_hat_KF[0] = temp_mhi_alone_KF.T[0] + (velocity*np.cos(temp_mhi_alone_KF.T[2])*self.Dt) ## update step with previously estimated x-location and x-part of kinematic model
                                            
                    mhi_alone_hat_KF[1] = temp_mhi_alone_KF.T[1] + (velocity*np.sin(temp_mhi_alone_KF.T[2])*self.Dt) ## update step with previously estimated y-location and y-part of kinematic model
                    
                    mhi_alone_hat_KF[2] = temp_mhi_alone_KF.T[2] + angular*self.Dt ## update step with previously estimated yaw and yaw-part of kinematic model
                
                
              #To python 2  
              #  Sigma_alone_hat_KF = G_alone@temp_Sigma_alone_KF@G_alone.T + R_alone # update step of covariance
                Sigma_alone_hat_KF = np.dot(np.dot(G_alone,temp_Sigma_alone_KF),G_alone.T) + R_alone # update step of covariance
                                    
                Kalman_gain_alone = np.zeros((state, 3))
                
                
                #To python 2  
    #            Kalman_gain_alone = Sigma_alone_hat_KF @ H_alone.T @ (inv(H_alone @ Sigma_alone_hat_KF@ H_alone.T + Q_alone)) ## compute Kalman gain
                Kalman_gain_alone = np.linalg.multi_dot([Sigma_alone_hat_KF , H_alone.T , (inv(np.linalg.multi_dot([H_alone , Sigma_alone_hat_KF, H_alone.T]) + Q_alone))])
                
                z_alone, h_alone = np.zeros((H_alone.shape[0],1)), np.zeros((H_alone.shape[0],1))
                        
                z_alone = np.array([noisy_x,noisy_y , heading]) ## measurement vector contains the 2D GPS measurement and yaw measurement                     
                        
                h_alone = np.array([mhi_alone_hat_KF[0], mhi_alone_hat_KF[1], mhi_alone_hat_KF[2]]) ## vector h contains the 2D vector and yaw of update step  
               
                measurement_mat_alone = np.zeros((H_alone.shape[0],1))
                                                
                measurement_mat_alone = np.reshape(z_alone - h_alone, (H_alone.shape[0],1))
                                                
                aggreg_vec_alone = np.dot(Kalman_gain_alone , measurement_mat_alone)
                                                
                aggreg_matrix_alone = np.dot(Kalman_gain_alone,H_alone)
                                    
                mhi_alone_KF = mhi_alone_hat_KF + aggreg_vec_alone.T ## correction step for 2D location and yaw vector
                                        
                Sigma_alone_KF = Sigma_alone_hat_KF - np.dot(aggreg_matrix_alone,Sigma_alone_hat_KF) ## correction step for 3x3 covariance matrix
                        
                temp_X = mhi_alone_KF.T[0]
                temp_Y = mhi_alone_KF.T[1]
                                        
                ekf_error = norm(np.array([temp_X - true_X, temp_Y - true_Y])) ## location estimation error
                   
            else: ## if vehicle had neighbors the previous time instant, then reset single Kalman the current time instant
                        
                mhi_alone_KF[0] = noisy_x ## x-measurement of GPS
                mhi_alone_KF[1] = noisy_y ## y-measurement of GPS
                mhi_alone_KF[2] = heading ## yaw measurement from IMU
                
                Sigma_alone_KF = np.eye(3)
                                    
                temp_X = mhi_alone_KF[0]
                temp_Y = mhi_alone_KF[1]
                                        
                ekf_error = norm(np.array([temp_X - true_X, temp_Y - true_Y])) ## single Kalman-based location estimation error
        
        return ekf_error, mhi_alone_KF, Sigma_alone_KF, temp_X,temp_Y
    
    def Distributed_Lapl (self, neighbors_mat_current, neighbors_mat_previous, temp_mhi_local_KF, temp_Sigma_local_KF, idx_time, ind_Deg, delta_X, 
                          delta_Y, test_noise_X, test_noise_Y, test_true_X, test_true_Y, delta_X_from_neighbors, 
                          delta_Y_from_neighbors, neighborhood_heading, neighborhood_velocity, neighborhood_angular):
        
        ############ Input ############
        
        ############ neighbors_mat_current: vector of neighbors' indices for the current time instant ############
        ############ neighbors_mat_previous: vector of neighbors' indices for the previous time instant ############
        ############ temp_mhi_local_KF: location vector estimated by DCEKF the previous time instant ############
        ############ temp_Sigma_local_KF: covariance matrix estimated by DCEKF the previous time instant ############
        ############ idx_time: current time instant (scalar) ############
        ############ ind_Deg: number of neighbors of vehicle for the current time instant (scalar) ############
        ############ delta_X: x-differential coordinate of vehicle for the current time instant ############
        ############ delta_Y: y-differential coordinate of vehicle for the current time instant ############
        ############ test_noise_X: vector of GPS x-measurements for vehicle and neighbors for the current time instant ############
        ############ test_noise_Y: vector of GPS y-measurements for vehicle and neighbors for the current time instant ############
        ############ test_true_X: true x-location of vehicle for the current time instant ############
        ############ test_true_Y: true y-location of vehicle for the current time instant ############
        ############ delta_X_from_neighbors: x-differential coordinates from each neighbor corresponding to ego vehicle for the current time instant (vector) ############
        ############ delta_Y_from_neighbors: y-differential coordinates from each neighbor corresponding to ego vehicle for the current time instant (vector) ############
        ############ neighborhood_heading: yaw measurements for ego and neighbors for the current time instant (vector) ############
        ############ neighborhood_velocity: velocity measurements for ego and neighbors for the current time instant (vector) ############
        ############ neighborhood_angular: angular velocity measurements for ego and neighbors for the current time instant (vector) ############
        
        
        ############ Output ############
        
        ############ local_lapl_error: DLL-based location estimation error of vehicle for the current time instant ############
        ############ temp_ekf_local_error: DCEKF-based location estimation error of vehicle for the current time instant ############
        ############ mhi_local_KF: location vector estimated by DCEKF the current time instant ############
        ############ Sigma_local_KF: covariance matrix estimated by DCEKF the current time instant (matrix) ############
        
        
        #### Perform DLL (initializer)####
        
        ## formulate local Laplacin and Adjacency matrix which correspond to the star topology of ego and neighbors
        L_local = np.zeros( (2*int(ind_Deg + 1), int(ind_Deg + 1)) )
                        
        L_bottom = np.eye(int(ind_Deg + 1))
                        
        L_upper = np.eye(int(ind_Deg + 1))
                        
        L_upper[0,0] = int(ind_Deg)
                        
        L_upper[0, 1:] = -1
                        
        L_upper[1:, 0] = -1
                        
        A_local = np.zeros ( (int(ind_Deg + 1) , int(ind_Deg + 1)))
                        
        A_local[0, 1:] = 1
                        
        A_local[1:, 0] = 1
                        
        L_local = np.concatenate( (L_upper, L_bottom), axis = 0 )
        
        ## formulate local Laplacin and Adjacency matrix which correspond to the star topology of ego and neighbors
        
        
        ## construct the measurement vectors which contain GPS (of ego and neighbors) and range measurements between ego and neighbors and vice versa 
        
        temp_vec_b_x = np.zeros(L_local.shape[0]) 
        temp_vec_b_y = np.zeros(L_local.shape[0]) 
        
        temp_vec_b_x[0] = delta_X ## x-differential of ego
        
        temp_vec_b_x[1:int(ind_Deg) + 1] = delta_X_from_neighbors ## x-differential of neighbors corresponding to ego
        
        temp_vec_b_x[int(ind_Deg)  + 1] = test_noise_X[-1] ## x-GPS of ego
        
        temp_vec_b_x[int(ind_Deg)  + 2:] = test_noise_X[:-1] ## x-GPS transmitted by neighbors
        
        temp_vec_b_y[0] = delta_Y ## y-differential of ego
        
        temp_vec_b_y[1:int(ind_Deg)  + 1] = delta_Y_from_neighbors ## y-differential of neighbors corresponding to ego
        
        temp_vec_b_y[int(ind_Deg)  + 1] = test_noise_Y[-1] ## y-GPS of ego
        
        temp_vec_b_y[int(ind_Deg)  + 2:] = test_noise_Y[:-1] ## y-GPS transmitted by neighbors
        
        ## construct the measurement vectors which contain GPS (of ego and neighbors) and range measurements between ego and neighbors and vice versa
        
        x_final_local = np.zeros( int(ind_Deg + 1) )
                        
        y_final_local = np.zeros( int(ind_Deg + 1) )
                        
        x_final_local = lsmr(L_local, temp_vec_b_x)[0] ## estimate x-location of vehicles
        
        y_final_local = lsmr(L_local, temp_vec_b_y)[0] ## estimate y-location of vehicles
        
        #### Perform DLL (initializer)####
        
        #### Perform DCEKF ####
        
        
        ## Initialize quantities of DCEKF
        state = int(ind_Deg + 1) ## state = ego + number of neighbors
        mhi_local_KF = np.zeros(3*state) ## vector to be estimated shall contain the 2D location and yaw of vehicle and its neighbors (the first half is about x-location, the second half about y-location and the third about yaw)
        Sigma_local_KF = np.eye(3*state) ## size of covarinace derives from above
            
        mhi_hat_local_KF = np.zeros(3*state)
        Sigma_hat_local_KF = np.eye(3*state)
        ## Initialize quantities of DCEKF
        
        if (idx_time == 0): ##if simulation has just started
                                    
            mhi_local_KF = np.concatenate([x_final_local, y_final_local, neighborhood_heading]) ## estimated vector will contain the results from DLL and yaw measurements received from neighbors
               
            Sigma_local_KF = np.eye(3*state)
                    
            x_ekf_vector = mhi_local_KF[0] ## keep only the x-location of vehicle from the estimated vector
                        
            y_ekf_vector = mhi_local_KF[state] ## keep only the y-location of vehicle from the estimated vector       
        else:
                          
                    
            if (np.array_equal(neighbors_mat_current, neighbors_mat_previous) == False): ##if the set of neighbors the current time instant has modified with respect to the previous time, reset DCEKF
        
                mhi_local_KF = np.concatenate([x_final_local, y_final_local, neighborhood_heading]) ## estimated vector will contain the results from DLL and yaw measurements received from neighbors
                   
                Sigma_local_KF = np.eye(3*state)
                                        
                x_ekf_vector = mhi_local_KF[0] ## keep only the x-location of vehicle from the estimated vector
                        
                y_ekf_vector = mhi_local_KF[state] ## keep only the y-location of vehicle from the estimated vector
                
                
            else:
                
                for u in range(state): ##for each neighbor (containing vehicle)
                            
                    if (neighborhood_angular[u] != 0):
                        
                        mhi_hat_local_KF[u] = temp_mhi_local_KF.T[u] + (-(neighborhood_velocity[u])/(neighborhood_angular[u]))*np.sin(temp_mhi_local_KF.T[u+2*state]) + ((neighborhood_velocity[u] )/(neighborhood_angular[u]))*np.sin(temp_mhi_local_KF.T[u+2*state] + neighborhood_angular[u]*self.Dt)  ## update step of location vector with the previously estimated vector plus the x-kinematic model of neighbors
                                        
                        mhi_hat_local_KF[u+state] = temp_mhi_local_KF.T[u+state] + ((neighborhood_velocity[u])/(neighborhood_angular[u]))*np.cos(temp_mhi_local_KF.T[u+2*state]) - ((neighborhood_velocity[u] )/(neighborhood_angular[u]))*np.cos(temp_mhi_local_KF.T[u+2*state] + neighborhood_angular[u]*self.Dt) ## update step of location vector with the previously estimated vector plus the y-kinematic model of neighbors
                               
                        mhi_hat_local_KF[u+2*state] = temp_mhi_local_KF.T[u+2*state] +  neighborhood_angular[u]*self.Dt ## update step of location vector with the previously estimated vector plus the yaw-kinematic model of neighbors
                    
                    else:

                        mhi_hat_local_KF[u] = temp_mhi_local_KF.T[u] + (neighborhood_velocity[u]*np.cos(temp_mhi_local_KF.T[u+2*state])*self.Dt)  ## update step of location vector with the previously estimated vector plus the x-kinematic model of neighbors
                                        
                        mhi_hat_local_KF[u+state] = temp_mhi_local_KF.T[u+state] + (neighborhood_velocity[u]*np.sin(temp_mhi_local_KF.T[u+2*state])*self.Dt) ## update step of location vector with the previously estimated vector plus the y-kinematic model of neighbors
                               
                        mhi_hat_local_KF[u+2*state] = temp_mhi_local_KF.T[u+2*state] +  neighborhood_angular[u]*self.Dt ## update step of location vector with the previously estimated vector plus the yaw-kinematic model of neighbors
                    
                ## initialize corresponding matrices of Kalman   
                G_local = np.eye(3*state)  
                
                G_local = np.eye(3*state)  
        
                l = 2*state
                for u in range (state):
            
                    if (neighborhood_angular[u] != 0):
                                    
                        G_local[u,l] = ((neighborhood_velocity[u])/(neighborhood_angular[u]))*np.cos(temp_mhi_local_KF.T[u+2*state]) - ((neighborhood_velocity[u] )/(neighborhood_angular[u]))*np.cos(temp_mhi_local_KF.T[u+2*state] + neighborhood_angular[u]*self.Dt) 
                        G_local[u+state,l] = ((neighborhood_velocity[u])/(neighborhood_angular[u]))*np.sin(temp_mhi_local_KF.T[u+2*state]) - ((neighborhood_velocity[u] )/(neighborhood_angular[u]))*np.sin(temp_mhi_local_KF.T[u+2*state] + neighborhood_angular[u]*self.Dt)
                                    
                    else:
                                    
                        G_local[u,l] = (-neighborhood_velocity[u])*np.sin(temp_mhi_local_KF.T[u+2*state])*self.Dt
                        G_local[u+state,l] = (neighborhood_velocity[u])*np.cos(temp_mhi_local_KF.T[u+2*state])*self.Dt
                            
                    l += 1   
                    
                R_local = np.eye(3*state)
                    
                Q_local = np.eye(5*state)
                ## initialize corresponding matrices of Kalman
                
                Sigma_hat_local_KF = np.linalg.multi_dot([G_local,temp_Sigma_local_KF , G_local.T ])+ R_local ## update step of covariance matrix
                                        
                Kalman_gain = np.zeros((3*state, 5*state))
                
                ## create the block diagonal matrix H, containing local Laplacian matrix
                H = np.zeros((Kalman_gain.shape[1], Kalman_gain.shape[0]))
                                    
                H[:2*state,:state] = np.copy(L_local)
                                    
                H[2*state:4*state,state:2*state] = np.copy(L_local)
                    
                H[4*state:5*(state),2*state:] = np.eye(state)
                
                ## create the block diagonal matrix H, containing local Laplacian matrix
                
                Kalman_gain = np.linalg.multi_dot([Sigma_hat_local_KF, H.T , (inv(np.linalg.multi_dot([H , Sigma_hat_local_KF , H.T]) + Q_local))]) ## compute Kalman gain
                     
                z, h = np.zeros((H.shape[0],1)), np.zeros((H.shape[0],1))
                            
                z = np.concatenate([temp_vec_b_x, temp_vec_b_y, neighborhood_heading])
                                
                h_delta_X = np.zeros(state)
                                
                h_delta_Y = np.zeros(state)
                        
                for u in range(state):
                    for l in range(state):
                        if (A_local[u,l] == 1):
                            h_delta_X[u] += mhi_hat_local_KF[u] - mhi_hat_local_KF[l]
                            h_delta_Y[u] += mhi_hat_local_KF[u+state] - mhi_hat_local_KF[l+state]
                                            
                h = np.concatenate([h_delta_X, mhi_hat_local_KF[:state], h_delta_Y, mhi_hat_local_KF[state:2*state], mhi_hat_local_KF[2*state:]])
           
                measurement_mat = np.zeros((H.shape[0],1))
                                            
                measurement_mat = np.reshape(z - h, (H.shape[0],1))
                       
                aggreg_vec = np.dot(Kalman_gain , measurement_mat)
                                                    
                aggreg_matrix = np.dot(Kalman_gain,H)
                       
                mhi_local_KF = (mhi_hat_local_KF + aggreg_vec.T)[0] ## correction step of location vector
                                            
                Sigma_local_KF = Sigma_hat_local_KF -np.dot(aggreg_matrix,Sigma_hat_local_KF)  ## correction step of covariance matrix
                        
                x_ekf_vector = mhi_local_KF[0] ## keep only the x-location of vehicle from the estimated vector
                        
                y_ekf_vector = mhi_local_KF[state] ## keep only the y-location of vehicle from the estimated vector
          
        
        #### Perform DCEKF ####
        
 
        local_lapl_error = norm(np.array([test_true_X - x_final_local[0] , test_true_Y - y_final_local[0]]), 2) # DLL-based location estimation error
            
        temp_ekf_local_error = norm(np.array([test_true_X - x_ekf_vector , test_true_Y - y_ekf_vector]), 2) # DCEKF-based location estimation error
            
        return local_lapl_error, temp_ekf_local_error, mhi_local_KF, Sigma_local_KF,x_ekf_vector , y_ekf_vector
    
    def Initialize_Parameter_for_Location_Estimation(self, neighbors_mat, idx_of_individual, true_X, true_Y, sigma_d, sigma_a, sigma_x, sigma_y, ind_Deg, heading, velocity, angular, noisy_x, noisy_y):
        
        ############ Input ############
        
        ############ neighbors_mat: vector of neighbors' indices for the current time instant ############
        ############ idx_of_individual: index of chosen vehicle ############
        ############ true_X: true x-locations of vehicles the current time instant ############
        ############ true_Y: true y-locations of vehicles the current time instant ############
        ############ sigma_d: std of distance measurement noise (scalar) ############
        ############ sigma_a: std of distance measurement noise (scalar) ############
        ############ sigma_x: std of GPS x-measurement noise (scalar) ############
        ############ sigma_y: std of GPS y-measurement noise (scalar) ############
        ############ ind_Deg: number of neighbors of vehicle for the current time instant (scalar) ############
        ############ heading: yaw of vehicles for the current time instant (vector) ############
        ############ velocity: velocity of vehicles for the current time instant (vector) ############
        ############ angular: angular velocity of vehicles for the current time instant (vector) ############
        
        ############ Output ############
        
        ############ delta_X: x-differential coordinate of vehicle for the current time instant (scalar) ############
        ############ delta_Y: y-differential coordinate of vehicle for the current time instant (scalar) ############
        ############ test_Points: true 2D location vector of vehicle for the current time instant ############
        ############ test_noise_Points: 2D GPS measurements of vehicle and neighbors for the current time instant (matrix) ############
        ############ delta_X_from_neighbors: x-differential coordinates from each neighbor corresponding to ego vehicle for the current time instant (vector) ############
        ############ delta_Y_from_neighbors: y-differential coordinates from each neighbor corresponding to ego vehicle for the current time instant (vector) ############
        ############ neighborhood_heading: yaw measurements for ego and neighbors for the current time instant (vector) ############
        ############ neighborhood_velocity: velocity measurements for ego and neighbors for the current time instant (vector) ############
        ############ neighborhood_angular: angular velocity measurements for ego and neighbors for the current time instant (vector) ############
        
        neighborhood_heading = np.zeros(int(ind_Deg + 1))
        
        neighborhood_heading[0] = heading[idx_of_individual]
        
        neighborhood_velocity = np.zeros(int(ind_Deg + 1))
        
        neighborhood_velocity[0] = velocity[idx_of_individual]
        
        neighborhood_angular = np.zeros(int(ind_Deg + 1))
        
        neighborhood_angular[0] = angular[idx_of_individual]
        u = 1
        for i in neighbors_mat:
    
            neighborhood_heading[u] = heading[i]
            neighborhood_velocity[u] = velocity[i]
            neighborhood_angular[u] = angular[i]
            
                            
            u += 1
                            
        test_Points = np.zeros(2)
        test_noise_Points = np.zeros((int(ind_Deg + 1) , 2))
       
        u = 0
        neighbors_gps_noise = []
        for i in neighbors_mat: ## for each neighbor 
            
            ## receive from each neighbor their 2D GPS measurement and store it to matrix
            test_noise_Points[u,0] = true_X[i] + np.random.normal(0,self.sigma_x) ## GPS x-measurement
            test_noise_Points[u,1] = true_Y[i] + np.random.normal(0,self.sigma_y) ## GPS y-measurement
         
            gps_noise=round(np.linalg.norm(np.array([true_X[i][0],true_Y[i][0]])-np.array([test_noise_Points[u,0],test_noise_Points[u,1]])),2)
            neighbors_gps_noise.append(gps_noise)
            
            u += 1
        
        ## add to matrix the 2D GPS measurement of vehicle
        test_noise_Points[-1,0] =noisy_x #true_X[idx_of_individual] + np.random.normal(0,sigma_x) ## GPS x-measurement
        test_noise_Points[-1,1] = noisy_y#+ np.random.normal(0,sigma_y) ## GPS y-measurement
        
        test_Points[0] = true_X[idx_of_individual] ## true x-location of vehicle
        test_Points[1] = true_Y[idx_of_individual] ## true y-location of vehicle
        
        delta_X = 0
        
        delta_Y = 0
        
        u = 0
        for j in neighbors_mat[:,0]: ## for each neighbor 
            ## compute relative distance
            n_d = norm(np.array([true_X[idx_of_individual]-true_X[j],true_Y[idx_of_individual]-true_Y[j]]),2) +np.random.normal(0,self.sigma_d)
            
              
            ## compute relative azimuth agle          
            n_azim = self.CalculateAzimuthAngle(true_X[idx_of_individual], true_X[j], true_Y[idx_of_individual], true_Y[j]) + np.random.normal(0,math.radians(self.sigma_a))
                
            ## compute differential coordinates        
            delta_X += -abs(n_d)*math.sin(abs(n_azim)) ## x-differential
            delta_Y += -abs(n_d)*math.cos(abs(n_azim)) ## y-differential 
                       
            u += 1
       
        delta_X_from_neighbors = np.zeros(int(ind_Deg)) ## vector containing range measurement (for x) from each neighbor corresponding to the vehicle
        delta_Y_from_neighbors = np.zeros(int(ind_Deg)) ## vector containing range measurement (for y) from each neighbor corresponding to the vehicle
        
        ind_vec = np.zeros(2)
        
        u = 0
        for j in neighbors_mat[:,0]:           
            
            
            ### find the neighbors of each neighbor of ego
            list_of_neighbors = []
            
            for i in range (true_X.shape[0]):
                d = norm(np.array([true_X[j]-true_X[i], true_Y[j]-true_Y[i]]),2)
                
                if (i != j):
                    
                    if (d <= self.neighbors_threshold and d > 0):
                        
                        list_of_neighbors.append(i)
                        
                    
            
            ## range measurements association : neighbor transmits its own vector of range measurements -> find which of them correspond to ego vehicle
            temp_d = norm( np.array([ (test_noise_Points[u, 0] - test_noise_Points[-1, 0]) , (test_noise_Points[u, 1] - test_noise_Points[-1, 1]) ]) )
            
            temp_az = self.CalculateAzimuthAngle(test_noise_Points[u, 0], test_noise_Points[-1, 0], test_noise_Points[u, 1], test_noise_Points[-1, 1])
            
            ind_vec[0] = -temp_d*math.sin(temp_az)
            ind_vec[1] = -temp_d*math.cos(temp_az)
            
            target_vec = np.zeros((2, len(list_of_neighbors) ))
            
            l = 0
            
            for k in list_of_neighbors:
                
                temp_d = norm(np.array([true_X[j] - true_X[k], true_Y[j] - true_Y[k]])) + np.random.normal(0, self.sigma_d)
                
                temp_az = self.CalculateAzimuthAngle(true_X[j],  true_X[k], true_Y[j], true_Y[k]) + np.random.normal(0, math.radians(self.sigma_a))
                
                target_vec[0, l] = -temp_d*math.sin(temp_az)
                                    
                target_vec[1, l] = -temp_d*math.cos(temp_az)
                
                l += 1
            
            score_x = np.zeros( ( len(list_of_neighbors) ))
            score_y = np.zeros( ( len(list_of_neighbors) ))
            
            for l in range (score_x.shape[0]):
                
                # if ( ( (ind_vec[0])*(target_vec[0,l]) ) > 0 ):
                                           
                #     score_x[l] = norm( abs(ind_vec[0]) - abs(target_vec[0,l]) )
                                            
                # else:
                                            
                #     score_x[l] = 100
                                            
                # if ( ( (ind_vec[1])*(target_vec[1,l]) ) > 0):
                                            
                                            
                #     score_y[l] = norm( abs(ind_vec[1]) - abs(target_vec[1,l]) )
                # else:
                                           
                #     score_y[l] = 100

                score_x[l] = norm( (ind_vec) - (target_vec[:,l]) )
                                        
            # if (np.mean(score_x) == 100):
                                    
            #     for l in range (score_x.shape[0]):
                                        
            #         score_x[l] = norm( abs(ind_vec[0]) - abs(target_vec[0,l]) )
                                        
            # if (np.mean(score_y) == 100):
                                    
            #     for l in range (score_y.shape[0]):
                                        
            #         score_y[l] = norm( abs(ind_vec[1]) - abs(target_vec[1,l]) )  
            
            
            a=np.argwhere( np.min(score_x) == score_x )
            b=list_of_neighbors[a[0][0]]
            
            flag_for_range_measurements_x = np.copy(list_of_neighbors[ np.argwhere( np.min(score_x) == score_x )[0,0] ]) ## determine which range measurement (for x) of j correspond to ego vehicle
            flag_for_range_measurements_y = np.copy(list_of_neighbors[ np.argwhere( np.min(score_x) == score_x )[0,0] ]) ## determine which range measurement (for x) of j correspond to ego vehicle       
            
            ## range measurements association
            
            temp_d = norm(np.array([true_X[j] - true_X[flag_for_range_measurements_x], true_Y[j] - true_Y[flag_for_range_measurements_y]])) + np.random.normal(0, self.sigma_d) ## distance corresponding to j-i measured by j
            
            temp_az = self.CalculateAzimuthAngle(true_X[j],  true_X[flag_for_range_measurements_x], true_Y[j], true_Y[flag_for_range_measurements_y]) + np.random.normal(0, math.radians(self.sigma_a)) ## azimuth angle corresponding to j-i measured by j
            
            delta_X_from_neighbors[u] =  -temp_d*math.sin(temp_az)
            
            delta_Y_from_neighbors[u] =  -temp_d*math.cos(temp_az)
            
            u += 1    
        
        return delta_X, delta_Y, test_Points, test_noise_Points, delta_X_from_neighbors, delta_Y_from_neighbors, neighborhood_heading, neighborhood_velocity, neighborhood_angular,neighbors_gps_noise
    
    def Initialize_Kalman_with_zeros (self, time_instances):
        
        ### Initialize Kalman quantities with zero ###
        
        mhi_local_KF = np.zeros((time_instances), dtype=np.object)
        
        Sigma_local_KF = np.zeros((time_instances), dtype=np.object)
    
        neighbors_mat = np.zeros((time_instances), dtype=np.object)
        
        mhi_alone_KF = np.zeros((time_instances), dtype=np.object)
        
        Sigma_alone_KF = np.zeros((time_instances), dtype=np.object)
        
        neighbors_mat = np.zeros((time_instances), dtype=np.object)
        
        ### Initialize Kalman quantities with zero ###
        
        return mhi_local_KF, Sigma_local_KF, mhi_alone_KF, Sigma_alone_KF, neighbors_mat
    
    def Initialize_Kalman (self,  Deg, A, idx_of_individual):
    
        neighbors = np.zeros(int(Deg[-1] + 1 ))
            
        neighbors[-1] = idx_of_individual
        
        neighbors_mat= np.zeros((int(Deg[-1])), dtype=np.object)
            
        neighbors_mat= neighbors_mat.astype(float)
        
        neighbors[:-1] = np.argwhere( (1 == A) ).T ## find neighbors of chosen vehicle from adjacency matrix A
           
        neighbors_mat = np.argwhere( (1 == A) ) ## create the matrix of neighbors for each time instant
            
        mhi_local_KF = np.zeros((3*int(Deg[-1] + 1)), dtype=np.object) ## distributed Kalman estimates the 2D location of chosen vehicle and neighbors
            
        mhi_local_KF = mhi_local_KF.astype(float)
        
            
        Sigma_local_KF= np.zeros((3*int(Deg[-1] + 1), 3*int(Deg[-1] + 1)), dtype=np.object) ## size of covariance derives from the above
           
        Sigma_local_KF = Sigma_local_KF.astype(float)
        
        mhi_alone_KF = np.zeros(3, dtype = np.object)

        mhi_alone_KF = mhi_alone_KF.astype(float)
        Sigma_alone_KF = np.zeros((3,3), dtype = np.object)

        Sigma_alone_KF = Sigma_alone_KF.astype(float)
        return mhi_local_KF, Sigma_local_KF, mhi_alone_KF, Sigma_alone_KF, neighbors_mat
    
    def Initialize_Location_and_Control (self, time_instances, number_of_vehicles):
        
        ### Initialize with zeros ###
        true_X = np.zeros((number_of_vehicles, time_instances))
        true_Y = np.zeros((number_of_vehicles, time_instances))
        
        vel_X = np.zeros((true_X.shape[0], time_instances))
        vel_Y = np.zeros((true_X.shape[0], time_instances))
        
        ang_vel_Z = np.zeros((true_X.shape[0], time_instances))
        
        true_heading = np.zeros((true_X.shape[0], time_instances))
        
        heading = np.zeros((true_X.shape[0], time_instances))
       
        velocity = np.zeros((true_X.shape[0], time_instances))
        angular = np.zeros((true_X.shape[0], time_instances))
        ### Initialize with zeros ###
        
        A = np.zeros((true_X.shape[1], true_X.shape[0]))
        
        Deg = np.zeros(A.shape[0])
        
        return true_X, true_Y, vel_X, vel_Y, ang_vel_Z, velocity, angular, true_heading, heading, A, Deg
    
    def Create_Location_Velocity_Ang_Vel_Heading(self, locations, time_instances, step, idx_time, true_X, true_Y, vel_X, vel_Y, ang_vel_Z, velocity, angular, heading, true_heading):
    
       
        for i in range (true_X.shape[0]):
            
            #step = 0
            print ('i: ', i)
            
            true_X[i] = np.copy(locations[i+step,1])
            true_Y[i] = np.copy(locations[i+step,2])
            
            vel_X[i] = np.copy(locations[i+step,5])
            vel_Y[i] = np.copy(locations[i+step,6])
            
            ang_vel_Z[i] = (np.copy(locations[i+step,13]))
            
            heading[i] = math.radians(np.copy(locations[i+step,4])) + np.random.normal(0,math.radians(0.2))
            
            true_heading[i] = math.radians(np.copy(locations[i+step,4]))
            
            n_vel_x = np.random.normal(0, 0.1*abs(vel_X[i]))
            
            n_vel_y = np.random.normal(0, 0.1*abs(vel_Y[i]))
            
            velocity[i] = math.sqrt( (vel_X[i] + n_vel_x)**2 + (vel_Y[i] + n_vel_y)**2 ) ### add IMU noise to velocity 
           
            angular[i] = ang_vel_Z[i] + np.random.normal(0, math.radians(0.2))
                
            
        
        return true_X, true_Y, angular, velocity, heading
    
    
    def Create_Adjacency_and_Degree (self, data, idx_of_individual):
    
        neighbors=[]
        
        #get x, y
        true_X_Y=data[:,1:3]
        
        
        A_temp = np.zeros(true_X_Y.shape[0])
        dist=(true_X_Y-true_X_Y[idx_of_individual])**2
        dist_total=dist.sum(axis=1)        
        dist_total=np.sqrt(dist_total) 
        neighbor_idxs=np.where((dist_total<self.neighbors_threshold) & (dist_total>0))
        A_temp[neighbor_idxs]=1
        neighbors=data[neighbor_idxs]
#        print(neighbors)
        

        
    
        
        
        
#        u = 0
#        for neighbor_x, neighbor_y,v_id in zip(true_X,true_Y,vehicle_ids):
#       # for k in range (true_X.shape[0]):
#            d = norm(np.array(ego_x-neighbor_x, ego_y-neighbor_y),2)
#                
#            if (d <= self.neighbors_threshold and d > 0): ### communication range between the chosen and any other vehicle = 20 m
#                    
#                A_temp[u] = 1
#                
#            u += 1
#               
                        
        deg = np.sum(A_temp)
        
        
                
        return A_temp, deg, neighbors,neighbor_idxs

    
    
def main():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('ext_kalman', anonymous=True)
    Estimator()
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    main()

