# ROS node for EKF cooperative localization algorithm
This project provides a ROS wrapper for the EKF cooperative localization algorithm 
## Prerequisites
- ROS1 installed
- If you don't use rosbags Carla and the same version of carla ros-bridge must be installed
- python libraries: numpy and scipy installed

## Build
The code has been tested in Ubuntu 20.04 with ROS noetic installed <br/>
Create a catkin workspace

- Create a ROS workspace and a source directory

        mkdir -p ~/ros-workspace/src
        

- Clone the package into the source directory . 

        cd ~/ros-workspace/src
        git clone https://gitlab.com/isi_athena_rc/cpsosaware/cooperative-localization-and-tracking/ros_ekf.git`

- Install dependencies

        cd ~/ros-workspace 
        sudo apt-get update && rosdep update
        rosdep install --from-paths src --ignore-src -r -y


- Build the packages

        cd ~/ros-workspace && catkin_make

- Configure ROS library Path

        source ~/ros-workspace/devel/setup.bash


## Run

1. Start a ros core
        `roscore`
2. Start the ekf node
        `rosrun extended_kalman node.py`
3. Provide data<br/>
You can either provide data by utilizing the **rosbag** provided
        `rosbag play ~/ros-workspace/ros_ekf/rosbags/10m.bag` <br/>
or by **using Carla and carla ros-bridge**<br/><br/>
        Navigate to Carla folder
        './CarlaJUE4.sh`<br/>
        Start ros bridge<br/>
        'roslaunch carla_ros_bridge carla_ros_bridge_with_example_ego_vehicle`


### 1 Provide data
You can either publish the necessary data 
#### With rosbags

#### With Carla

### 2 Start the ROS node

### 3 Visualize the path
